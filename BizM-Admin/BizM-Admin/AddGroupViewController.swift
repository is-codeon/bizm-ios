//
//  AddGroupViewController.swift
//  BizM-Admin
//
//  Created by Dipen Panchasara on 23/02/18.
//  Copyright © 2018 ICPL. All rights reserved.
//

import UIKit
import FirebaseDatabase

let GroupNameMinLength = 3
let GroupNameMaxLength = 25

class AddGroupViewController: UIViewController {
    
    @IBOutlet weak var groupNameTextField:UITextField!
    @IBOutlet weak var saveButtonItem:UIBarButtonItem!
    var activityIndicator: UIActivityIndicatorView?
    
    var dbRef: DatabaseReference!
    var objOrg:Organisation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Add Group"
        dbRef = Database.database().reference().child("Groups")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveGroup(_ sender:Any){
        self.view.isUserInteractionEnabled = false
        self.view.endEditing(true)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let refreshBarButton: UIBarButtonItem = UIBarButtonItem(customView: activityIndicator!)
        self.navigationItem.rightBarButtonItem = refreshBarButton
        activityIndicator?.startAnimating()
        
        let key = dbRef.childByAutoId().key
        let dict:[String : String] = [kId: key, kName : groupNameTextField.text ?? "", kOrgId : (self.objOrg?.id)!]
        self.dbRef.child(key).setValue(dict) { (error, db) in
            if error == nil {
                self.activityIndicator?.stopAnimating()
                print("Group successfully added.")
                self.navigationController?.popViewController(animated: true)
            } else {
                print("Group adding fail.")
                self.view.isUserInteractionEnabled = true
            }
        }
    }
}

extension AddGroupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, text.isEmpty {
            saveButtonItem.isEnabled = true
        } else {
            saveButtonItem.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let newString = (text as NSString).replacingCharacters(in: range, with: string)

        if newString.isEmpty && (newString.count < GroupNameMinLength || newString.count > GroupNameMaxLength) {
            saveButtonItem.isEnabled = false
        } else {
            saveButtonItem.isEnabled = true
        }
        
        return newString.count < GroupNameMaxLength
    }
}
