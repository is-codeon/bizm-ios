//
//  AddOrganisationViewController.swift
//  BizM-Admin
//
//  Created by Shashi Chunara on 23/02/18.
//  Copyright © 2018 ICPL. All rights reserved.
//

import UIKit
import FirebaseDatabase

let OrganisationNameMinLength = 3
let OrganisationNameMaxLength = 25

class AddOrganisationViewController: UIViewController {
    
    @IBOutlet weak var orgNameTextField:UITextField!
    @IBOutlet weak var saveButtonItem:UIBarButtonItem!
    var dbRef: DatabaseReference!
    var activityIndicator: UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Add Organisation"
        dbRef = Database.database().reference().child("Organisation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveOrganisation(_ sender:Any){
        self.view.isUserInteractionEnabled = false
        self.view.endEditing(true)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let refreshBarButton: UIBarButtonItem = UIBarButtonItem(customView: activityIndicator!)
        self.navigationItem.rightBarButtonItem = refreshBarButton
        activityIndicator?.startAnimating()

        let key = dbRef.childByAutoId().key
        let dict:[String : String] = [kId: key, kName : orgNameTextField.text ?? ""]
        self.dbRef.child(key).setValue(dict) { (error, db) in
            if error == nil {
                self.activityIndicator?.stopAnimating()
                print("Organisation successfully added.")
                self.navigationController?.popViewController(animated: true)
            } else {
                print("Organisation adding fail.")
                self.view.isUserInteractionEnabled = true
            }
        }
    }
}

extension AddOrganisationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, text.isEmpty {
            saveButtonItem.isEnabled = true
        } else {
            saveButtonItem.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        
        if newString.isEmpty && (newString.count < OrganisationNameMinLength || newString.count > OrganisationNameMaxLength) {
            saveButtonItem.isEnabled = false
        } else {
            saveButtonItem.isEnabled = true
        }
        
        return newString.count < OrganisationNameMaxLength
    }
}
