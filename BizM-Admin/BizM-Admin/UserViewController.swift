//
//  UserViewController.swift
//  BizM-Admin
//
//  Created by Dipen Panchasara on 23/02/18.
//  Copyright © 2018 ICPL. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct User {
    let id  : String
    let name: String
    let groups: [[String:Any]]
    let orgId: [String]
}

class UserViewController: UIViewController {
    
    var allUsers = [User]()
    var selectedIndex:Int = -1
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator:UIActivityIndicatorView!
    @IBOutlet weak var infoLabel:UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Users"
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllUsers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getAllUsers() {
        self.tableView.isHidden = true
        self.indicator.startAnimating()
        infoLabel.text = "Pulling User(s)..."

        let userRef = Database.database().reference().child("Users")
        self.allUsers.removeAll()
        userRef.observeSingleEvent(of: .value) { (snapshot) in
            if !snapshot.exists() {
                // Show No User message
                self.infoLabel.text = "No User(s) Found."
                self.indicator.stopAnimating()
                return
            }
            if let users = snapshot.value as? [String:Any] {
                for key in users.keys {
                    if let obj = users[key] as? [String:Any]{
                        if let uName = obj[kName] as? String, let uGroups = obj["groups"] as? [[String : Any]], let uOrgId = obj["organisations"] as? [String] {
                            self.allUsers.append(User(id: key, name:uName , groups: uGroups , orgId: uOrgId))
                        }
                        
                    }
                }
                
                if self.allUsers.count > 0 {
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    self.infoLabel.text = ""
                } else {
                    // Show No Groups message
                    self.infoLabel.text = "No User(s) Found."
                }
                self.indicator.stopAnimating()
            }            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? AddUserViewController {
            if selectedIndex != -1 {
                viewController.objUser = allUsers[selectedIndex]
            }
        }
    }
}

extension UserViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "userCellIdentifier")
        
        cell.textLabel?.text = allUsers[indexPath.row].name
        cell.detailTextLabel?.text = allUsers[indexPath.row].id
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "EditSegue", sender: self)
    }
}
