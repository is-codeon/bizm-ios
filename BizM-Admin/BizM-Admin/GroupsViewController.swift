//
//  SecondViewController.swift
//  BizM-Admin
//
//  Created by Dipen Panchasara on 23/02/18.
//  Copyright © 2018 ICPL. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct Group {
    var id : String
    let name: String
    let ordId: String
}

let kId = "id"
let kName = "name"
let kOrgId = "orgId"

class GroupsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var groupTableView:UITableView!
    @IBOutlet weak var indicator:UIActivityIndicatorView!
    @IBOutlet weak var infoLabel:UILabel!
    
    var dbRef:DatabaseReference!
    var arrGroups = [Group]()
    var objOrg:Organisation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = objOrg?.name
        dbRef = Database.database().reference().child("Groups")
        self.groupTableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGroupsForOrganisation()
//        dbRef.observe(.childAdded) { (snapshot) in
//            if let obj = snapshot.value as? [String:Any] {
//                if obj[kOrgId] as! String == self.orgId {
//                    self.arrGroups.contains(where: <#T##(Group) throws -> Bool#>)
//                    self.arrGroups.append(Group(id: obj[kId] as! String, name: obj[kName] as! String, ordId: obj[kOrgId] as? String ?? ""))
//                    self.groupTableView.reloadData()
//                    self.groupTableView.isHidden = false
//                    self.infoLabel.text = ""
//                }
//            }
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        dbRef.removeAllObservers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    func getGroups() {
        self.groupTableView.isHidden = true
        infoLabel.text = "Pulling Groups..."
        self.indicator.startAnimating()
        dbRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let results = snapshot.value as? [String:Any] {
                self.arrGroups.removeAll()
                for key in results.keys {
                    if let obj = results[key] as? [String:Any] {
                        self.arrGroups.append(Group(id: obj[kId] as! String, name: obj[kName] as! String, ordId: obj[kOrgId] as! String))
                    }
                }
                if results.count > 0 {
                    // Show table if 1 or more groups
                    DispatchQueue.main.async {
                        self.groupTableView.reloadData()
                        self.groupTableView.isHidden = false
                        self.infoLabel.text = ""
                    }
                } else {
                    self.infoLabel.text = "No Group(s) Found"
                }
            }
            DispatchQueue.main.async {
                self.indicator.stopAnimating()
            }
        })
    }
    */
    
    func getGroupsForOrganisation(){
        self.groupTableView.isHidden = true
        self.indicator.startAnimating()
        infoLabel.text = "Pulling Group(s)..."
        dbRef.observeSingleEvent(of: .value, with: { (snapshot) in
//        dbRef.observe(.value, with: { (snapshot) in
            if !snapshot.exists() {
                // Show No Groups message
                self.infoLabel.text = "No Group(s) Found."
                self.indicator.stopAnimating()
                return
            }

            var temp = [Group]()
            if let results = snapshot.value as? [String:Any] {
                self.arrGroups.removeAll()
                for key in results.keys {
                    if let obj = results[key] as? [String:Any] {
                        temp.append(Group(id: obj[kId] as! String, name: obj[kName] as! String, ordId: obj[kOrgId] as? String ?? ""))
                    }
                }
            }
            // Filter Groups based on OrgId
            let result = temp.filter({ (obj) -> Bool in
                if obj.ordId == self.objOrg?.id { return true }
                return false
            })
            DispatchQueue.main.async {
                if result.count > 0 {
                    self.arrGroups = result
                    // Sort Groups by Name
                    self.arrGroups.sort(by: { (group1, group2) -> Bool in
                        group1.name < group2.name
                    })
                    self.groupTableView.reloadData()
                    self.groupTableView.isHidden = false
                    self.infoLabel.text = ""
                } else {
                    // Show No Groups message
                    self.infoLabel.text = "No Group(s) Found."
                }
                self.indicator.stopAnimating()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addGroupSegue" {
            if let viewController = segue.destination as? AddGroupViewController {
                viewController.objOrg = self.objOrg
            }
        }
    }
    
    // MARK: Delegate
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGroups.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "groupCell") as UITableViewCell!
        
        // set the text from the data model
        cell.textLabel?.text = self.arrGroups[indexPath.row].name
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped \(self.arrGroups[indexPath.row]).")
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

