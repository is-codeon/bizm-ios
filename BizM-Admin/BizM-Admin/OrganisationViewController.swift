//
//  FirstViewController.swift
//  BizM-Admin
//
//  Created by Dipen Panchasara on 23/02/18.
//  Copyright © 2018 ICPL. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct Organisation {
    let id  : String
    let name: String
}

class OrganisationViewController: UIViewController {
    
    var allOrgs = [Organisation]()
    var selectedIndex:Int = -1
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator:UIActivityIndicatorView!
    @IBOutlet weak var infoLabel:UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Organisations"
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        getAllOrgs()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAllOrgs() {
        self.tableView.isHidden = true
        self.indicator.startAnimating()
        infoLabel.text = "Pulling Organisation(s)..."

        let orgRef = Database.database().reference().child("Organisation")
        orgRef.observeSingleEvent(of: .value) { (snapshot) in
            
            if !snapshot.exists() {
                // Show No Organisation message
                self.infoLabel.text = "No Organisation(s) Found."
                self.indicator.stopAnimating()
                return
            }
            
            self.allOrgs.removeAll()
            if let orgs = snapshot.value as? [String:Any] {
                for key in orgs.keys {
                    if let obj = orgs[key] as? [String:Any]{
                        self.allOrgs.append(Organisation(id: key, name: obj["name"] as! String))
                    }
                }
                if self.allOrgs.count > 0 {
                    // Sort Orgs by Name
                    self.allOrgs.sort(by: { (org1, org2) -> Bool in
                        org1.name < org2.name
                    })
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    self.infoLabel.text = ""
                } else {
                    // Show No Groups message
                    self.infoLabel.text = "No Organisation(s) Found."
                }
                self.indicator.stopAnimating()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? GroupsViewController {
            if selectedIndex > -1 {
                viewController.objOrg = allOrgs[selectedIndex]
                self.tabBarController?.tabBar.isHidden = true
            }
        }
    }
}

extension OrganisationViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allOrgs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "orgCellIdentifier")
        
        cell.textLabel?.text = allOrgs[indexPath.row].name
        cell.detailTextLabel?.text = allOrgs[indexPath.row].id
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped \(allOrgs[indexPath.row]).")
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "groupSegue", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
}

