//
//  AddUserViewController.swift
//  BizM-Admin
//
//  Created by Shashi Chunara on 23/02/18.
//  Copyright © 2018 ICPL. All rights reserved.
//

import UIKit
import CZPicker
import FirebaseDatabase

class AddUserViewController: UIViewController {
    
    @IBOutlet weak var userNameTextfield:UITextField!
    @IBOutlet weak var phoneNumberTextfield:UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButtonItem:UIBarButtonItem!
    
    @IBOutlet weak var orgButton: UIButton!
    var allOrgs = [Organisation]()
    var allGroups = [Group]()
    var groupsOfOrg = [Group]()
    var selectedOrg:Organisation?
    var selectedGroups = [[String:Any]]()
    var objUser:User?
    
    var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getAllOrgs()
        getAllGroups()
        
        if let user = objUser  {
            phoneNumberTextfield.text = user.id
            userNameTextfield.text = user.name
            phoneNumberTextfield.isEnabled = false
            userNameTextfield.isEnabled = false
            selectedGroups = user.groups
            self.title = "Edit User"
        } else {
            self.title = "Add User"
        }
        
        self.tableView.tableFooterView = UIView()
    }
    
    func getAllOrgs() {
        
        let orgRef = Database.database().reference().child("Organisation")
        self.allOrgs.removeAll()
        orgRef.observeSingleEvent(of: .value) { (snapshot) in
            if let orgs = snapshot.value as? [String:Any] {
                for key in orgs.keys {
                    if let obj = orgs[key] as? [String:Any]{
                        self.allOrgs.append(Organisation(id: key, name: obj["name"] as! String))
                        if self.objUser != nil {
                            self.selectedOrg = self.allOrgs.filter{
                                if $0.id == self.objUser?.orgId[0]{
                                    return true
                                }
                                return false
                                }.first
                            self.orgButton.setTitle(self.selectedOrg?.name, for: .normal)
                        }
                    }
                }
                self.orgButton.isEnabled = true
            }
        }
    }
    
    func getAllGroups() {
        
        let orgRef = Database.database().reference().child("Groups")
        self.allGroups.removeAll()
        orgRef.observeSingleEvent(of: .value) { (snapshot) in
            if let orgs = snapshot.value as? [String:Any] {
                for key in orgs.keys {
                    if let obj = orgs[key] as? [String:Any]{
                        self.allGroups.append(Group(id: key, name: obj["name"] as! String, ordId: obj["orgId"] as! String))
                        if self.objUser != nil {
                            self.showGroups()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func saveButtonClicked(_ sender: UIBarButtonItem) {
        self.view.isUserInteractionEnabled = false
        self.view.endEditing(true)

        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let refreshBarButton: UIBarButtonItem = UIBarButtonItem(customView: activityIndicator!)
        self.navigationItem.rightBarButtonItem = refreshBarButton
        activityIndicator?.startAnimating()
        
        let userRef = Database.database().reference().child("Users")
        
        let dict:[String : Any] = [kName: userNameTextfield.text ?? "User", "groups" : selectedGroups, "organisations" : [selectedOrg?.id]]
        userRef.child(phoneNumberTextfield.text!).setValue(dict) { (error, db) in
            if error == nil {
                print("User successfully added.")
                self.navigationController?.popViewController(animated: true)
            } else {
                print("User adding fail.")
                self.view.isUserInteractionEnabled = true
            }
        }
        
    }
    
    @IBAction func OrgButtonClicked(_ sender:UIButton) {
        let picker = CZPickerView(headerTitle: "Organisations", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.headerTitleColor = UIColor.black
        picker?.headerBackgroundColor = UIColor.lightGray
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = false
        picker?.allowMultipleSelection = false
        picker?.show()
    }
    
    func isAdminValueChanged(cell:GroupCell) {
        
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        if cell.accessoryType == .none {
            tableView(tableView, didSelectRowAt: indexPath)
        }
        
        selectedGroups = selectedGroups.map{
            var dict = $0
            if let groupId = dict["groupId"] as? String, groupId == groupsOfOrg[indexPath.row].id {
                if let isAdmin = dict["isAdmin"] as? Bool, isAdmin == true {
                    dict["isAdmin"] = false
                } else {
                    dict["isAdmin"] = true
                }
            }
            return dict
        }
    }
    
    func showGroups() {
        groupsOfOrg = self.allGroups.filter({ (obj) -> Bool in
            if obj.ordId == self.selectedOrg?.id { return true }
            return false
        })
        self.tableView.isHidden = false
        self.tableView.reloadData()
    }
}

protocol GroupCellDelegate: class {
    func isAdminValueChanged(cell: GroupCell)
}

class GroupCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var switchAdmin: UISwitch!
    var delegate: GroupCellDelegate?
    
    @IBAction func switchValueChanged(_ sender:UISwitch) {
        delegate?.isAdminValueChanged(cell: self)
    }
    
}

extension AddUserViewController: UITableViewDelegate, UITableViewDataSource, GroupCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupsOfOrg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupCellIdentifier") as! GroupCell
        
        cell.titleLabel.text = groupsOfOrg[indexPath.row].name
        cell.delegate = self
        
        if selectedGroups.contains(where: { element in
            if let groupId = element["groupId"] as? String, groupId == groupsOfOrg[indexPath.row].id {
                if let isAdmin = element["isAdmin"] as? Bool, isAdmin == true{
                    cell.switchAdmin.isOn = true
                }
                return true
            }
            return false
        }) {
            cell.accessoryType = .checkmark
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
            
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
            
            selectedGroups = selectedGroups.filter({ $0["groupId"] as? String != groupsOfOrg[indexPath.row].id })
//            print(pets.count)
//            selectedGroups = selectedGroups.filter{
//                if let groupId = $0["groupId"] as? String, groupId != groupsOfOrg[indexPath.row].id {return true}
//                return false
//            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            let dictGroup:[String:Any] = [
                "groupId" : groupsOfOrg[indexPath.row].id,
                "isAdmin" : false,
                "organisation" : selectedOrg?.id ?? "Org1"
            ]
            selectedGroups.append(dictGroup)
        }
    }
}

extension AddUserViewController: CZPickerViewDelegate, CZPickerViewDataSource {
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return allOrgs.count
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return allOrgs[row].name
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int) {
        
        selectedOrg = allOrgs[row]
        orgButton.setTitle(selectedOrg?.name, for: .normal)
        showGroups()
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func czpickerViewDidClickCancelButton(_ pickerView: CZPickerView!) {
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [Any]!) {
        for row in rows {
            
            if let row = row as? Int {
                print(allOrgs[row].name)
            }
        }
    }
}

extension AddUserViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if let text = textField.text, text.isEmpty {
//            saveButtonItem.isEnabled = true
//        } else {
//            saveButtonItem.isEnabled = false
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        
        if textField == phoneNumberTextfield && newString.count > 10 {
            return false
        }
        
        if textField == userNameTextfield && newString.isEmpty && (newString.count < 5 || newString.count > 20) {
            saveButtonItem.isEnabled = false
        } else if textField == phoneNumberTextfield && newString.isEmpty && newString.count < 10 {
            saveButtonItem.isEnabled = false
        } else {
            saveButtonItem.isEnabled = true
        }
        
        return newString.count < 20
    }
}

